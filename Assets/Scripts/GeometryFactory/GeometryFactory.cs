﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal */
/* 
 * 
 * The GeometryFactory takes care of all geometric objects which are commonly used in the linear Algebra.
 * 
 */

public class GeometryFactory : MonoBehaviour {

	// Create CoordinateSystem
	public static CoordinateSystem CreateCoordinateSystem(Vector3 coordinate, int index){
		CoordinateSystem coordinateSystem = new CoordinateSystem (coordinate, index);
		return coordinateSystem;
	}

	// Create Point
	public static Point CreatePoint(Vector3 coordinate){
		Point point = new Point (coordinate);
		return point;
	}

	// Create Line
	public static Line CreateLineFromPoints(Vector3 pointA, Vector3 pointB){
		Line line = new Line (pointA, pointB - pointA);
		return line;
	}

	public static Line CreateLineFromEquation(Vector3 positionVector, Vector3 directionVector){
		Line line = new Line (positionVector, directionVector);
		return line;
	}

	// Create Plane
	public static Plane CreatePlaneFromPoints(Vector3 pointA, Vector3 pointB, Vector3 pointC){
		Vector3 normalVector = Vector3.Cross (Vector3.Normalize(pointB - pointA), Vector3.Normalize(pointC - pointA));
		Plane plane = new Plane (pointA, pointB - pointA, pointC - pointA, normalVector);
		return plane;
	}

	public static Plane CreatePlaneFromEquation(Vector3 positionVector, Vector3 directionVectorA, Vector3 directionVectorB){
		Vector3 normalVector = Vector3.Cross (directionVectorA, directionVectorB);
		Plane plane = new Plane (positionVector, directionVectorA, directionVectorB, normalVector);
		return plane;
	}

	public static Plane CreatePlaneFromTwoLines(Line lineA, Line lineB){
		// TODO
		// Intersection between the two lines for positionVector
		return null;
	}

	// Create Sphere
	// TODO

}
