using UnityEngine;
using System.Collections;
using Vectrosity;
using SaveState;

/* @ Marco Pleines - Hochschule Rhein-Waal */
/* @ Simon Jürgensmeyer - Hochschule Rhein-Waal */


public class Plane : GeometryObject, SelectAble, DrawAble
{

		// Member
		private Vector3 positionVector = new Vector3 ();
		private Vector3 directionVectorA = new Vector3 ();
		private Vector3 directionVectorB = new Vector3 ();
		private Vector3 normalVector = new Vector3 ();
		private float lineWidth = 0.5f;
		private float scale = 1f;
		private int gridSubdivisions = 100;
		private GameObject go = new GameObject ("Plane");
		private Color color = Color.red;
		private Color selectBoxColor = Color.green;
		private Material lineMaterial = null;
		private VectorLine lineSelectBox;
		private bool selected = false;
	
		// Constructor
		public Plane (Vector3 positionVector, Vector3 directionVectorA, Vector3 directionVectorB, Vector3 normalVector)
		{
				this.positionVector = positionVector;
				this.directionVectorA = directionVectorA;
				this.directionVectorB = directionVectorB;
				this.normalVector = normalVector;
		}

		public Plane(){
		}

		// Getter
		public Vector3 GetPositionVector ()
		{
				return this.positionVector;
		}

		public Vector3 GetDirectionVectorA ()
		{
				return this.directionVectorA;
		}

		public Vector3 GetDirectionVectorB ()
		{
				return this.directionVectorB;
		}

		public Vector3 GetNormalVector ()
		{
				return this.normalVector;
		}

		public float GetLineWidth ()
		{
				return this.lineWidth;
		}
	
		public float GetScale ()
		{
				return this.scale;
		}
	
		public string GetName ()
		{
				return this.go.name;
		}

		public Color GetSelectBoxColor()
		{
				return this.selectBoxColor;
		}

	
		public Color GetColor ()
		{
				return this.color;
		}
	
		public Material GetLineMaterial ()
		{
				return this.lineMaterial;
		}

		public int GetGridSubdivisions ()
		{
				return gridSubdivisions;
		}

		public GameObject GetGameObject(){
				return this.go;
		}

		public bool IsSelected(){
				return selected;
		}

	
		// Setter
		public void SetPositionVector (Vector3 positionVector)
		{
				this.positionVector = positionVector;
		}

		public void SetDirectionVectorA (Vector3 directionVector)
		{
				this.directionVectorA = directionVector;
		}

		public void SetDirectionVectorB (Vector3 directionVector)
		{
				this.directionVectorB = directionVector;	 
		}

		public void SetScale (float scale)
		{
				this.scale = scale;
		}
	
		public void SetName (string name)
		{
				this.go.name = name;
		}
	
		public void SetColor (Color color)
		{
				this.color = color;
		}

		public void SetSelectBoxColor(Color color)
		{
			this.color = color;
		}

		public void SetMaterial (Material material)
		{
				this.lineMaterial = material;
		}

		public void SetGridSubdivisions (int subdivisions)
		{
				this.gridSubdivisions = subdivisions;
		}


	#region Saveable implementation
		public SaveStateBundle SaveState ()
		{
			SaveStateBundle stateBundle = new SaveStateBundle ();
			stateBundle.SetBundleIdentifier ("Plane");
			stateBundle.PutVector3 ("position", positionVector);
			stateBundle.PutVector3 ("direction_a", directionVectorA);
			stateBundle.PutVector3 ("direction_b", directionVectorB);
			return stateBundle;
		}

		public void RestoreState (SaveStateBundle bundle)
		{
			positionVector = bundle.GetVector3("position");
			directionVectorA = bundle.GetVector3 ("direction_a");
			directionVectorB = bundle.GetVector3 ("direction_b");
		}
	#endregion

	#region DrawAble implementation
		public void Draw ()
		{
				Vector3[] lineAPoints = new [] {
					positionVector,
					positionVector + directionVectorA
				};
		
				Vector3[] lineBPoints = new [] {
					positionVector,
					positionVector + directionVectorB
				};
		
				VectorLine lineA = new VectorLine ("Direction Vector A", lineAPoints, null, 1);
				VectorLine lineB = new VectorLine ("Direction Vector B", lineBPoints, null, 1);
				lineA.vectorObject.transform.parent = go.transform;
				lineB.vectorObject.transform.parent = go.transform;
		
				lineA.SetColor (Color.magenta);
				lineB.SetColor (Color.magenta);
		
				lineA.Draw3DAuto ();
				lineB.Draw3DAuto ();
		
				for (int i = 1; i <= gridSubdivisions; i++) {
						Vector3[] lineAPointsGrid = new [] {
							positionVector + directionVectorA / gridSubdivisions * i,
							positionVector + directionVectorB + (directionVectorA / gridSubdivisions * i)
						};

						Vector3[] lineBPointsGrid = new [] {
							positionVector + directionVectorB / gridSubdivisions * i,
							positionVector + directionVectorA + (directionVectorB / gridSubdivisions * i)
						};
			
						VectorLine lineAGrid = new VectorLine ("Grid Line A", lineAPointsGrid, null, 1);
						lineAGrid.vectorObject.transform.parent = go.transform;
						lineAGrid.SetColor (Color.gray);
						lineAGrid.Draw3DAuto ();
			
						VectorLine lineBGrid = new VectorLine ("Grid Line B", lineBPointsGrid, null, 1);
						lineBGrid.vectorObject.transform.parent = go.transform;
						lineBGrid.SetColor (Color.gray);
						lineBGrid.Draw3DAuto ();
				}
		}
	#endregion

	#region GeometryObject implementation
		public string Name {
				get {
						//TODO implement
						return null;
				}
				set {
						//TODO implement
				}
		}
	#endregion

	public void Select() {
		Vector3[] cubePoints = new Vector3[] {
			new Vector3(-0.2f, 0f, 0f) + positionVector, new Vector3(+0.2f, 0f, 0f) + positionVector,
			new Vector3(-0.2f, 0f, 0f) + positionVector, new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorA,
			new Vector3(-0.2f, 0f, 0f) + positionVector, new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorB,
			new Vector3(+0.2f, 0f, 0f) + positionVector, new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorA,
			new Vector3(+0.2f, 0f, 0f) + positionVector, new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorB,
			new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorA, new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorA,
			new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorB, new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorB,
			new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorB, new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorA +directionVectorB,
			new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorA, new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorA +directionVectorB,
			new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorB, new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorA +directionVectorB,
			new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorA, new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorA +directionVectorB,
			new Vector3(+0.2f, 0f, 0f) + positionVector + directionVectorA +directionVectorB, new Vector3(-0.2f, 0f, 0f) + positionVector + directionVectorA +directionVectorB};
		lineSelectBox = new VectorLine("Select", cubePoints, selectBoxColor, lineMaterial, 2.0f);
		lineSelectBox.vectorObject.transform.parent = go.transform;
		lineSelectBox.vectorObject.transform.position = go.transform.position;
		lineSelectBox.Draw3DAuto();
		this.selected = true;
	}
	
	public void DeSelect() {
		lineSelectBox.StopDrawing3DAuto();
		this.selected = false;
	}
}
