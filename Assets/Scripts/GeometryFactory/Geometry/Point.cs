﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using SaveState;

/* @ Marco Pleines - Hochschule Rhein-Waal */

public class Point : GeometryObject, SelectAble, DrawAble {

	// Member
	private Vector3 coordinate = new Vector3 (0,0,0);
	private float lineWidth = 0.5f;
	private float scale = 1f;
	private GameObject go = new GameObject("Point");
	private Color color = Color.red;
	private Color selectBoxColor = Color.green;
	private Material lineMaterial = null;
	private VectorLine lineSelectBox;
	private bool selected = false;


	public Point(){
	}

	// Constructor
	public Point(Vector3 coordinate){
		this.coordinate = coordinate;
		go.transform.position = coordinate;
	}

	public Point(Vector3 coordinate, string name){
		this.coordinate = coordinate;
		this.Name = name;
	}


	// Getter
	public Vector3 GetCoordinate(){
		return this.coordinate;
	}

	public float GetLineWidth(){
		return this.lineWidth;
	}

	public float GetScale(){
		return this.scale;
	}

	public Color GetColor(){
		return this.color;
	}

	public Color GetSelectBoxColor(){
		return this.selectBoxColor;
	}

	public Material GetLineMaterial(){
		return this.lineMaterial;
	}

	public GameObject GetGameObject(){
		return this.go;
	}

	public bool IsSelected(){
		return selected;
	}

	// Setter
	public void SetCoordinate(Vector3 coordinate){
		this.coordinate = coordinate;
	}

	public void SetLineWidth(float lineWidth){
		this.lineWidth = lineWidth;
	}

	public void SetScale(float scale){
		this.scale = scale;
		this.coordinate = this.coordinate * scale;
	}

	public void SetName(string name){
		this.go.name = name;
	}

	public void SetColor(Color color){
		this.color = color;
	}

	public void SetSelectBoxColor(Color color){
		this.selectBoxColor = color;
	}

	public void SetMaterial(Material material){
		this.lineMaterial = material;
	}

	#region Saveable implementation
	public SaveStateBundle SaveState ()
	{
		SaveStateBundle stateBundle = new SaveStateBundle ();
		stateBundle.SetBundleIdentifier ("Point");
		stateBundle.PutVector3 ("coordinate", coordinate);
		return stateBundle;
	}

	public void RestoreState (SaveStateBundle bundle)
	{
		coordinate = bundle.GetVector3 ("coordinate");
	}
	#endregion
	
	#region DrawAble implementation
	public void Draw ()
	{
		// point of line bottom left corner
		Vector3 bottomLeft = this.coordinate;
		bottomLeft[0] = bottomLeft[0] - this.scale;
		bottomLeft[1] = bottomLeft[1] - this.scale;
		// point of line top right corner
		Vector3 topRight = this.coordinate;
		topRight [0] = topRight [0] + this.scale;
		topRight [1] = topRight [1] + this.scale;
		// point of line top left corner
		Vector3 topLeft = this.coordinate;
		topLeft [0] = topLeft [0] - this.scale;
		topLeft [1] = topLeft [1] + this.scale;
		// point of line bottom right corner
		Vector3 bottomRight = this.coordinate;
		bottomRight [0] = bottomRight [0] + this.scale;
		bottomRight [1] = bottomRight [1] - this.scale;
		
		// Line of drawing point 1
		VectorLine linesForPoint = new VectorLine ("point", new Vector3[] {bottomLeft, topRight, topLeft, bottomRight}, lineMaterial, lineWidth, LineType.Discrete);
		linesForPoint.vectorObject.transform.parent = go.transform;
		linesForPoint.SetColor (color);
		linesForPoint.Draw3DAuto ();
	}
	#endregion
	
	#region GeometryObject implementation
	public string Name {
		get {
			//TODO implement
			return null;
		}
		set {
			//TODO implement
		}
	}
	#endregion

	public void Select() {
		Vector3[] cubePoints = new Vector3[] {new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, -0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate , new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, -0.5f) + coordinate, new Vector3(0.5f, 0.5f, -0.5f) + coordinate, new Vector3(-0.5f, 0.5f, -0.5f) + coordinate, new Vector3(-0.5f, -0.5f, -0.5f) + coordinate, new Vector3(-0.5f, 0.5f, -0.5f) + coordinate, new Vector3(0.5f, 0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, -0.5f) + coordinate, new Vector3(-0.5f, -0.5f, -0.5f) + coordinate, new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(-0.5f, -0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate};
		lineSelectBox = new VectorLine("Select", cubePoints, selectBoxColor, lineMaterial, 2.0f);
		lineSelectBox.vectorObject.transform.parent = go.transform;
		// buggy, needs to be fixed
		lineSelectBox.vectorObject.transform.localPosition = coordinate*-1;
		lineSelectBox.Draw3DAuto();
		this.selected = true;
	}
	
	public void DeSelect() {
		lineSelectBox.StopDrawing3DAuto();
		this.selected = false;
	}
}
