﻿using UnityEngine;
using System.Collections;
using Vectrosity;


public class SelectBox : MonoBehaviour {

	private GameObject go = new GameObject ("SelectBox");
	private Material lineMaterial = null;
	private GameObject target = GameObject.FindGameObjectWithTag("MainCamera");
	private GameObject sel = GameObject.Find("Vector Select");

	private VectorLine line;
	
	// Use this for initialization
	void Start () {

	}


	// Update is called once per frame
	void Update () {
		//sel.transform.LookAt(target.transform);
		//sel.transform.Rotate(new Vector3(0,180,0));
	}

	public void Select(Vector3 coordinate) {
		
		// Make a Vector3 array that contains points for a cube that's 1 unit in size
		Vector3[] cubePoints = new Vector3[] {new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, -0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate , new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, -0.5f) + coordinate, new Vector3(0.5f, 0.5f, -0.5f) + coordinate, new Vector3(-0.5f, 0.5f, -0.5f) + coordinate, new Vector3(-0.5f, -0.5f, -0.5f) + coordinate, new Vector3(-0.5f, 0.5f, -0.5f) + coordinate, new Vector3(0.5f, 0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, -0.5f) + coordinate, new Vector3(-0.5f, -0.5f, -0.5f) + coordinate, new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(-0.5f, -0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, -0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate};
		//Vector3[] cubePoints = new Vector3[] {new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, -0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(0.5f, 0.5f, 0.5f) + coordinate, new Vector3(-0.5f, 0.5f, 0.5f) + coordinate};
		
		// Make a line using the above points and material, with a width of 2 pixels
		line = new VectorLine("Select", cubePoints, Color.green, lineMaterial, 2.0f);
		line.vectorObject.transform.parent = go.transform;
		
		
		line.Draw3DAuto();
		//go.AddComponent (typeof(CoordinateSystemLookAt));
		
		// Make this transform have the vector line object that's defined above
		// This object is a rigidbody, so the vector object will do exactly what this object does
		//VectorManager.ObjectSetup (gameObject, line, Visibility.Dynamic, Brightness.None);

	}

	public void DeSelect() {
		line.StopDrawing3DAuto();
	}
}
