using UnityEngine;
using System.Collections;
using Vectrosity;
using System.Collections.Generic;
using System;
using SaveState;

/* @ Simon Jürgensmeyer, Marco Pleines - Hochschule Rhein-Waal */

public class CoordinateSystem{

		// Private Member
		// size
		private int coordinateSystemSizeXplus = 5;
		private int coordinateSystemSizeXminus = 3;
		private int coordinateSystemSizeYplus = 10;
		private int coordinateSystemSizeYminus = 8;
		private int coordinateSystemSizeZplus = 7;
		private int coordinateSystemSizeZminus = 9;
		private float coordinateFragmentScale = 1;
		// position
		private Vector3 coordinate = new Vector3 (0, 0, 0);
		// parent gameObjects
		private GameObject go = new GameObject ("CoordinateSystem");
		private GameObject csObjects = new GameObject ("CSObjects");
		// references
		private BoxCollider interactionScopeCollider;
		private int index;
		public List<GeometryObject> geometryObjects = new List<GeometryObject> ();
		// visuals
		private Color xAxisColor = Color.red;
		private Color yAxisColor = Color.green;
		private Color zAxisColor = Color.blue;
		private Font font;
		private Material fontMaterial;
		private GameObject textHolder = new GameObject ("TextMesh Holder");
		private float globalX;
		private float globalY;
		private float globalZ;
		private float lineLength;
		private GameObject MainCamera;
		private int fontSize = 36;
	

		// Constructor
		public CoordinateSystem (Vector3 coordinate, int index)
		{
				this.coordinate = coordinate;
				this.index = index;
				this.go.name = "CoordinateSystem" + index;
				this.go.transform.position = coordinate;
				// add a box collider which will determine the region of interaction
				this.go.AddComponent<BoxCollider> ();
				this.interactionScopeCollider =	this.go.GetComponent<BoxCollider> ();
				// the attribute is trigger defines, that other collider can move through this collider to check for collisions without blocking the other colliders
				this.interactionScopeCollider.isTrigger = true;
				this.interactionScopeCollider.size = new Vector3 (this.coordinateSystemSizeXplus*2, this.coordinateSystemSizeYplus*2, this.coordinateSystemSizeZplus*2);
				// add InteractionScope Component
				this.go.AddComponent<InteractionScope> ();
		}

		// This method will link an GeometryObject to the coordinate system by moving to to the appropriate
		// position in the coordinateSystem and unlink it from possible coordinate systems.
		public void LinkGeometryObject (GeometryObject geometryObject)
		{
				geometryObjects.Add (geometryObject);
		}

	public void LinkGeometryObject (Point point)
	{
		geometryObjects.Add (point);
		GameObject pointGO = point.GetGameObject ();
		Vector3 saveGlobalTransform = pointGO.transform.position;
		pointGO.transform.parent = go.transform;
		pointGO.transform.localPosition = saveGlobalTransform;
	}
	
	public void LinkGeometryObject (Line line)
	{
		geometryObjects.Add (line);
		GameObject lineGO = line.GetGameObject ();
		Vector3 saveGlobalTransform = lineGO.transform.position;
		lineGO.transform.parent = go.transform;
		lineGO.transform.localPosition = saveGlobalTransform;
	}
	
	public void LinkGeometryObject (Plane plane)
	{
		geometryObjects.Add (plane);
		GameObject planeGO = plane.GetGameObject ();
		Vector3 saveGlobalTransform = planeGO.transform.position;
		planeGO.transform.parent = go.transform;
		planeGO.transform.localPosition = saveGlobalTransform;
	}

		public List<GeometryObject> GetGeometryObjects ()
		{
				return geometryObjects;
		}


		// Getter
		public float GetX ()
		{
				return coordinate.x;
		}

		public float GetY ()
		{
				return coordinate.y;
		}

		public float GetZ ()
		{
				return coordinate.z;
		}

		public Vector3 GetCoordinate ()
		{
				return this.coordinate;
		}

		public int[] GetCoordinateSystemSize ()
		{
				int[] size = new int[] {
			this.coordinateSystemSizeXplus, this.coordinateSystemSizeXminus,
			this.coordinateSystemSizeYplus, this.coordinateSystemSizeYminus,
			this.coordinateSystemSizeZplus, this.coordinateSystemSizeZminus
			};
				return size;
		}

		public int GetIndex ()
		{
				return this.index;
		}

		public Color GetXAxisColor ()
		{
				return this.xAxisColor;
		}

		public Color GetYAxisColor ()
		{
				return this.yAxisColor;
		}

		public Color GetZAxisColor ()
		{
				return this.zAxisColor;
		}

		public int GetCoordinateSystemSizeXplus(){
				return this.coordinateSystemSizeXplus;
		}

		public int GetCoordinateSystemSizeXminus(){
				return this.coordinateSystemSizeXminus;
		}

		public int GetCoordinateSystemSizeYplus(){
			return this.coordinateSystemSizeYplus;
		}
	
		public int GetCoordinateSystemSizeYminus(){
				return this.coordinateSystemSizeYminus;
		}

		public int GetCoordinateSystemSizeZplus(){
				return this.coordinateSystemSizeZplus;
		}
	
		public int GetCoordinateSystemSizeZminus(){
				return this.coordinateSystemSizeZminus;
		}

		public float GetCoordinateFragmentScale ()
		{
				return this.coordinateFragmentScale;
		}

		public int GetFontSize ()
		{
				return this.fontSize;
		}

		public GameObject GetRootGameObject(){
				return this.go;
		}

		public BoxCollider GetInteractionScopeCollider(){
				return this.interactionScopeCollider;
		}

		public void SetInteractionScopeColliderSize(Vector3 size){
				this.interactionScopeCollider.size = size;
		}

		// Setter
		public void SetCoordinate (Vector3 coordinate)
		{
				this.coordinate = coordinate;
		}

		// Set all axis to the same size
		public void SetCoordinateSystemSize (int size)
		{

				/*
		while(csObjects.transform.GetChildCount()>0){
			GameObject.Destroy(csObjects.transform.GetChild (0));
		}
		*/
				this.coordinateSystemSizeXplus = size;
				this.coordinateSystemSizeXminus = size;
				this.coordinateSystemSizeZplus = size;
				this.coordinateSystemSizeZminus = size;
				this.coordinateSystemSizeYplus = size;
				this.coordinateSystemSizeYminus = size;
		}

		public void SetCoordinateSystemSizeX (int size)
		{
				this.coordinateSystemSizeXplus = size;
				this.coordinateSystemSizeXminus = size;
		}

		public void SetCoordinateSystemSizeXplus (int size)
		{
				this.coordinateSystemSizeXplus = size;
		}

		public void SetCoordinateSystemSizeXminus (int size)
		{
				this.coordinateSystemSizeXminus = size;
		}

		public void SetCoordinateSystemSizeY (int size)
		{
				this.coordinateSystemSizeYplus = size;
				this.coordinateSystemSizeYminus = size;
		}

		public void SetCoordinateSystemSizeYplus (int size)
		{
				this.coordinateSystemSizeYplus = size;
		}

		public void SetCoordinateSystemSizeYminus (int size)
		{
				this.coordinateSystemSizeYminus = size;
		}

		public void SetCoordinateSystemSizeZ (int size)
		{
				this.coordinateSystemSizeZplus = size;
				this.coordinateSystemSizeZminus = size;
		}

		public void SetCoordinateSystemSizeZplus (int size)
		{
				this.coordinateSystemSizeZplus = size;
		}

		public void SetCoordinateSystemSizeZminus (int size)
		{
				this.coordinateSystemSizeZminus = size;
		}
	
		public void SetName (string name)
		{
				this.go.name = name;
		}

		public void SetXAxisColor (Color c)
		{
				this.xAxisColor = c;
		}

		public void SetYAxisColor (Color c)
		{
				this.yAxisColor = c;
		}

		public void SetZAxisColor (Color c)
		{
				this.zAxisColor = c;
		}

		public void SetFontSize (int i)
		{
				this.fontSize = i;
		}

		public void SetcoordinateFragmentScale (float f)
		{
				this.coordinateFragmentScale = f;
		}


	#region Saveable implementation
		public SaveStateBundle SaveState ()
		{
				//TODO implement
				return null;
		}
	#endregion

		public void Start ()
		{
				MainCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		}
	
	#region DrawAble implementation
		public void Draw ()
		{

				MainCamera = GameObject.FindGameObjectWithTag ("MainCamera");


				csObjects.transform.parent = go.transform;
				csObjects.transform.position = this.coordinate;
				textHolder.transform.parent = csObjects.transform;


				globalX = coordinate.x;
				globalY = coordinate.y;
				globalZ = coordinate.z;
		
				// Draw X, Y, Z Axis and set some parameters
				Vector3[] xAxisPoints = new [] {
						new Vector3 (globalX - coordinateSystemSizeXminus, globalY, globalZ),
						new Vector3 (coordinateSystemSizeXplus + globalX, globalY, globalZ)
				};
				Vector3[] yAxisPoints = new [] {
						new Vector3 (globalX, globalY - coordinateSystemSizeYminus, globalZ),
						new Vector3 (globalX, coordinateSystemSizeYplus + globalY, globalZ)
				};
				Vector3[] zAxisPoints = new [] {
						new Vector3 (globalX, globalY, globalZ - coordinateSystemSizeZminus),
						new Vector3 (globalX, globalY, coordinateSystemSizeZplus + globalZ)
				};
		
				VectorLine xAxisA = new VectorLine ("x axis positive", xAxisPoints, null, 1);
				VectorLine yAxisA = new VectorLine ("y axis positive", yAxisPoints, null, 1);
				VectorLine zAxisA = new VectorLine ("z axis positive", zAxisPoints, null, 1);
		
				xAxisA.SetColor (xAxisColor);
				yAxisA.SetColor (yAxisColor);
				zAxisA.SetColor (zAxisColor);
		
		
				xAxisA.vectorObject.transform.parent = csObjects.transform;
				yAxisA.vectorObject.transform.parent = csObjects.transform;
				zAxisA.vectorObject.transform.parent = csObjects.transform;
		
				xAxisA.Draw3DAuto ();
				yAxisA.Draw3DAuto ();
				zAxisA.Draw3DAuto ();

		
				// Draw Lines on each axis
				lineLength = 0.1f;
				// Entres on X-Axis
				for (float i = -coordinateSystemSizeXminus; i <= coordinateSystemSizeXplus; i += coordinateFragmentScale) {
						// Don't draw on 0,0,0
						if (i == 0) {
								continue;
						}
						VectorLine.SetLine3D (Color.yellow, new Vector3 (globalX + i, globalY - lineLength, globalZ), new Vector3 (globalX + i, globalY + lineLength, globalZ)).vectorObject.transform.parent = csObjects.transform;
				}
				// Entres on Y-Axis
				for (float i = -coordinateSystemSizeYminus; i <= coordinateSystemSizeYplus; i += coordinateFragmentScale) {
						// Don't draw on 0,0,0
						if (i == 0) {
								continue;
						}
						VectorLine.SetLine3D (Color.yellow, new Vector3 (globalX - lineLength, globalY + i, globalZ), new Vector3 (globalX + lineLength, globalY + i, globalZ)).vectorObject.transform.parent = csObjects.transform;
				}
				// Entres on Z-Axis
				for (float i = -coordinateSystemSizeZminus; i <= coordinateSystemSizeZplus; i += coordinateFragmentScale) {
						// Don't draw on 0,0,0
						if (i == 0) {
								continue;
						}
						VectorLine.SetLine3D (Color.yellow, new Vector3 (globalX, globalY - lineLength, globalZ + i), new Vector3 (globalX, globalY + lineLength, globalZ + i)).vectorObject.transform.parent = csObjects.transform;
				}

		}
	
		public void DrawAxisNumbers ()
		{
				// load font and material
				font = Resources.Load ("3dtext/ARIAL") as Font;
				fontMaterial = Resources.Load ("3dtext/3dTextMaterial") as Material;

				for (float i = -coordinateSystemSizeXminus; i <= coordinateSystemSizeXplus; i += coordinateFragmentScale) {
						// Don't draw on 0,0,0
						if (i == 0) {
								continue;
						}

						// Numbers on X-Axis
						GameObject textObjectX = new GameObject ("TextobjectX" + i);
						textObjectX.AddComponent (typeof(MeshRenderer));
						MeshRenderer mr = textObjectX.GetComponent (typeof(MeshRenderer)) as MeshRenderer;
						textObjectX.AddComponent (typeof(TextMesh));
						TextMesh tm = textObjectX.GetComponent (typeof(TextMesh)) as TextMesh;
						textObjectX.renderer.material = fontMaterial;
						textObjectX.renderer.material.color = Color.black;
						tm.font = font;
						tm.characterSize = 0.1f;
						tm.fontSize = fontSize;
						tm.text = "" + i;
						tm.color = Color.black;
						tm.anchor = TextAnchor.UpperCenter;
						textObjectX.transform.Translate (new Vector3 (globalX + i, globalY - lineLength, globalZ));
						textObjectX.transform.parent = textHolder.transform;
						textObjectX.AddComponent (typeof(CoordinateSystemLookAt));
				}
				for (float i = -coordinateSystemSizeYminus; i <= coordinateSystemSizeYplus; i += coordinateFragmentScale) {
						// Don't draw on 0,0,0
						if (i == 0) {
								continue;
						}
			
						// Numbers on Y-Axis
						GameObject textObjectY = new GameObject ("TextobjectY" + i);
						textObjectY.AddComponent (typeof(MeshRenderer));
						MeshRenderer mrY = textObjectY.GetComponent (typeof(MeshRenderer)) as MeshRenderer;
						textObjectY.AddComponent (typeof(TextMesh));
						TextMesh tmY = textObjectY.GetComponent (typeof(TextMesh)) as TextMesh;
						textObjectY.renderer.material = fontMaterial;
						tmY.font = font;
						textObjectY.renderer.material.color = Color.black;
						tmY.characterSize = 0.1f;
						tmY.fontSize = fontSize;
						tmY.text = "" + i;
						tmY.color = Color.black;
						tmY.anchor = TextAnchor.MiddleLeft;
						textObjectY.transform.Translate (new Vector3 (globalX + lineLength, globalY + i, globalZ));
						textObjectY.transform.parent = textHolder.transform;
						textObjectY.AddComponent (typeof(CoordinateSystemLookAt));
				}
				for (float i = -coordinateSystemSizeZminus; i <= coordinateSystemSizeZplus; i += coordinateFragmentScale) {
						// Don't draw on 0,0,0
						if (i == 0) {
								continue;
						}
			
						// Numbers on Z-Axis
						GameObject textObjectZ = new GameObject ("TextobjectZ" + i);
						textObjectZ.AddComponent (typeof(MeshRenderer));
						MeshRenderer mrZ = textObjectZ.GetComponent (typeof(MeshRenderer)) as MeshRenderer;
						textObjectZ.AddComponent (typeof(TextMesh));
						TextMesh tmZ = textObjectZ.GetComponent (typeof(TextMesh)) as TextMesh;
						textObjectZ.renderer.material = fontMaterial;
						tmZ.font = font;
						textObjectZ.renderer.material.color = Color.black;
						tmZ.characterSize = 0.1f;
						tmZ.fontSize = fontSize;
						tmZ.text = "" + i;
						tmZ.color = Color.black;
						tmZ.anchor = TextAnchor.MiddleLeft;
						textObjectZ.transform.Translate (new Vector3 (globalX, globalY - lineLength, globalZ + i));
						textObjectZ.transform.parent = textHolder.transform;
						textObjectZ.AddComponent (typeof(CoordinateSystemLookAt));
				}
		}
	



	#endregion
}
