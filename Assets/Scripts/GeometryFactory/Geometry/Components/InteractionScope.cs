﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal */

/*
 * The interaction scope is a region which is defined by a coordinate system's collider (Is Trigger).
 * Only within that specific region the user is able to manipulate the geometry of that coordinate system
 * 
 */

public class InteractionScope : MonoBehaviour {

	// Member
	GameObject controlElement;

	void Start(){

	}

	void OnTriggerEnter(Collider other){
		Debug.Log("Something entered my scope of interaction");
		Debug.Log (gameObject.name + " got entered!");
		NotificationCenter.DefaultCenter ().PostNotification (this, "EnableInterface", gameObject);
	}

	void OnTriggerExit(Collider other){
		Debug.Log ("Something exited my scope of interaction");
		NotificationCenter.DefaultCenter ().PostNotification (this, "DisableInterface", gameObject);
	}
}
