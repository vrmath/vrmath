﻿using UnityEngine;
using System.Collections;

public class CoordinateSystemLookAt : MonoBehaviour {

	GameObject target;

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.LookAt(target.transform);
		gameObject.transform.Rotate(new Vector3(0,180,0));

	}
}
