﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal */

public interface DrawAble {

	void Draw();

}
