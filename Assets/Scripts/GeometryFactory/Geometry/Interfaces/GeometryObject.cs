using System;
using SaveState;
using UnityEngine;

public interface GeometryObject: DrawAble, Saveable, SelectAble{

	string Name{ get; set; }

	GameObject GetGameObject();
	}

