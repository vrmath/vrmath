﻿using UnityEngine;
using System.Collections;

/* @ Simon Jürgensmeyer - Hochschule Rhein-Waal */

public interface SelectAble {

	void Select();
	void DeSelect();
}
