﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using SaveState;

/* @ Marco Pleines - Hochschule Rhein-Waal */
/* @ Simon Jürgensmeyer - Hochschule Rhein-Waal */

/*
 * TODO: Scale 
 */


public class Line : GeometryObject, DrawAble, SelectAble {

	// Member
	private Vector3 positionVector = new Vector3 (0,0,0);
	private Vector3 directionVector = new Vector3(0,0,0);
	private float lineWidth = 0.5f;
	private float scale = 1f;
	private GameObject go = new GameObject("Line");
	private Color color = Color.red;
	private Color selectBoxColor = Color.green;
	private Material lineMaterial = null;
	private VectorLine lineSelectBox;
	private bool selected = false;

	// private coordinateSystemRef TODO
	
	// Constructor
	public Line (Vector3 positionVector, Vector3 directionVector){
		this.positionVector = positionVector;
		this.directionVector = directionVector;
	}

	public Line(){
	}

	// Getter
	public Vector3 GetPositionVector(){
		return this.positionVector;
	}

	public Vector3 GetDirectionVector(){
		return this.directionVector;
	}

	public float GetLineWidth(){
		return this.lineWidth;
	}
	
	public float GetScale(){
		return this.scale;
	}
	
	public string GetName(){
		return this.go.name;
	}
	
	public Color GetColor(){
		return this.color;
	}

	public Color GetSelectBoxColor(){
		return this.selectBoxColor;
	}
		
	public Material GetLineMaterial(){
		return this.lineMaterial;
	}

	public GameObject GetGameObject(){
		return this.go;
	}

	public bool IsSelected(){
		return selected;
	}


	// Setter
	public void SetDirectionVector(Vector3 directionVector){
		this.directionVector = directionVector;
	}

	public void SetPositionVector(Vector3 positionVector){
		this.positionVector = positionVector;
	}

	public void SetScale(float scale){
		this.scale = scale;
	}
	
	public void SetName(string name){
		this.go.name = name;
	}
	
	public void SetColor(Color color){
		this.color = color;
	}

	public void SetSelectBoxColor(Color color){
		this.color = color;
	}
	
	public void SetMaterial(Material material){
		this.lineMaterial = material;
	}

	#region Saveable implementation

	public SaveState.SaveStateBundle SaveState ()
	{
		SaveStateBundle stateBundle = new SaveStateBundle ();
		stateBundle.SetBundleIdentifier ("Line");
		stateBundle.PutVector3 ("position", positionVector);
		stateBundle.PutVector3 ("direction", directionVector);
		return stateBundle;
	}

	public void RestoreState (SaveStateBundle bundle)
	{
		positionVector = bundle.GetVector3 ("position");
		directionVector = bundle.GetVector3 ("direction");
	}
	#endregion

	#region DrawAble implementation
	public void Draw ()
	{
		VectorLine line = new VectorLine ("line", new Vector3[] {
				positionVector,
				positionVector + directionVector},
				lineMaterial, lineWidth);

		line.vectorObject.transform.parent = go.transform;
		line.SetColor(this.color);
		line.Draw3DAuto ();
	}

	#endregion

	#region GeometryObject implementation

	public string Name {
		get {
			//TODO implement
			return null;
		}
		set {
			//TODO implement
		}
	}

	#endregion

	public void Select() {
		Vector3[] cubePoints = new Vector3[] {

			// Lines from Position Vector to Direction Vector
			new Vector3(0.0f, 0.2f,0.2f) + positionVector, new Vector3(0.0f, 0.2f,0.2f) + positionVector + directionVector,
			new Vector3(0.0f, 0.2f, -0.2f) + positionVector, new Vector3(0.0f, 0.2f, -0.2f) + positionVector + directionVector,
			new Vector3(0.0f, -0.2f,0.2f) + positionVector, new Vector3(0.0f, -0.2f, 0.2f) + positionVector + directionVector,
			new Vector3(0.0f, -0.2f, -0.2f) + positionVector, new Vector3(0.0f, -0.2f, -0.2f) + positionVector + directionVector,

			// Square at the Position Vector site
			new Vector3(0.0f, -0.2f, 0.2f) + positionVector, new Vector3(0.0f, -0.2f, -0.2f) + positionVector,
			new Vector3(0.0f, -0.2f, 0.2f) + positionVector, new Vector3(0.0f, 0.2f, 0.2f) + positionVector,
			new Vector3(0.0f, -0.2f, -0.2f) + positionVector, new Vector3(0.0f, 0.2f, -0.2f) + positionVector,
			new Vector3(0.0f, 0.2f, -0.2f) + positionVector, new Vector3(0.0f, 0.2f, 0.2f) + positionVector,

			// Square at the Direction Vector site
			new Vector3(0.0f, -0.2f, 0.2f) + positionVector+ directionVector, new Vector3(0.0f, -0.2f, -0.2f) + positionVector+ directionVector,
			new Vector3(0.0f, -0.2f, 0.2f)+ positionVector + directionVector, new Vector3(0.0f, 0.2f, 0.2f) + positionVector+ directionVector,
			new Vector3(0.0f, -0.2f, -0.2f) + positionVector+ directionVector, new Vector3(0.0f, 0.2f, -0.2f) + positionVector+ directionVector,
			new Vector3(0.0f, 0.2f, -0.2f)+ positionVector + directionVector, new Vector3(0.0f, 0.2f, 0.2f) + positionVector + directionVector
			};

		lineSelectBox = new VectorLine("Select", cubePoints, selectBoxColor, lineMaterial, 2.0f);
		lineSelectBox.vectorObject.transform.parent = go.transform;
		lineSelectBox.vectorObject.transform.position = go.transform.position;

		lineSelectBox.Draw3DAuto();

		this.selected = true;
	}
	
	public void DeSelect() {
		lineSelectBox.StopDrawing3DAuto();
		this.selected = false;
	}
}