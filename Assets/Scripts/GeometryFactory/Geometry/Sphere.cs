﻿using UnityEngine;
using System.Collections;
using SaveState;

/* @ Marco Pleines - Hochschule Rhein-Waal */

public class Sphere : GeometryObject, DrawAble, SelectAble {

	// Member
	private GameObject go;

	// Constructor
	public Sphere (){
	}

	// Setter

	public GameObject GetGameObject(){
		return this.go;
	}

	// Getter


	#region Saveable implementation

	public SaveState.SaveStateBundle SaveState ()
	{
		//TODO implement
		return null;
	}

	public void RestoreState (SaveStateBundle bundle)
	{
		//TODO implement
	}
	#endregion

	#region DrawAble implementation
	public void Draw ()
	{
		// Create empty GameObject to hold vectorObjects
		GameObject sphere = new GameObject ("Sphere");
	}
	#endregion

	#region SelectAble implementation

	public void Select(){
		// TODO
	}

	public void DeSelect(){
		// TODO
	}

	#endregion


	#region GeometryObject implementation

	public string Name {
		get {
			//TODO implement
			return null;
		}
		set {
			throw new System.NotImplementedException ();
		}
	}

	#endregion
}
