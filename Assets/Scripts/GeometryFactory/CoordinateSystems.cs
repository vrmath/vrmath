﻿using UnityEngine;
using System.Collections;

public class CoordinateSystems : MonoBehaviour {

	public static CoordinateSystem cs0;
	public static CoordinateSystem cs1;
	public static CoordinateSystem cs2;

	void Awake(){
		cs0 = GeometryFactory.CreateCoordinateSystem(new Vector3(6.75f,1,-9f), 0);
		cs1 = GeometryFactory.CreateCoordinateSystem(new Vector3(-6.75f,1,-9f), 1);
		cs2 = GeometryFactory.CreateCoordinateSystem(new Vector3(-20f,1,-9f), 2);
	}

	// Use this for initialization
	void Start () {

		// Coordinate System on the left
		cs0.SetFontSize(18);
		cs0.SetcoordinateFragmentScale(0.5f);
		cs0.SetCoordinateSystemSizeX(3);
		cs0.SetCoordinateSystemSizeZ(6);
		cs0.SetCoordinateSystemSizeYplus(6);
		cs0.SetCoordinateSystemSizeYminus(1);
		cs0.Draw();
		cs0.DrawAxisNumbers();
		cs0.SetInteractionScopeColliderSize (new Vector3(6,12,12));

		// CS on the middle
		cs1.SetFontSize(18);
		cs1.SetcoordinateFragmentScale(0.5f);
		cs1.SetCoordinateSystemSizeX(3);
		cs1.SetCoordinateSystemSizeZ(6);
		cs1.SetCoordinateSystemSizeYplus(6);
		cs1.SetCoordinateSystemSizeYminus(1);
		cs1.Draw();
		cs1.DrawAxisNumbers();
		cs0.SetInteractionScopeColliderSize (new Vector3(6,12,12));

		// CS on the right
		cs2.SetFontSize(18);
		cs2.SetcoordinateFragmentScale(0.5f);
		cs2.SetCoordinateSystemSizeX(3);
		cs2.SetCoordinateSystemSizeZ(6);
		cs2.SetCoordinateSystemSizeYplus(6);
		cs2.SetCoordinateSystemSizeYminus(1);
		cs2.Draw();
		cs2.DrawAxisNumbers();
		cs0.SetInteractionScopeColliderSize (new Vector3(6,12,12));
	}
}
