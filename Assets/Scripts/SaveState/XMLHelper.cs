﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Xml;
using SaveState;

public class XMLHelper {

	private string xmlName;
	private XmlDocument document;
	private XmlElement coordinateSystem;
	private XmlElement meta;
	
	
	public void InitDocument(string name){
		xmlName = "Save/"+ name + ".xml";
		document = new XmlDocument();

		XmlDeclaration xmlDeclaration = document.CreateXmlDeclaration("1.0", "UTF-8", null);
		XmlElement root = document.DocumentElement;
		document.InsertBefore(xmlDeclaration,root);
			
		coordinateSystem = document.CreateElement( string.Empty, "coordinatesystem", string.Empty );
		coordinateSystem.SetAttribute("id",name);
		document.AppendChild(coordinateSystem);
		
		meta = document.CreateElement( string.Empty, "meta", string.Empty );
		coordinateSystem.AppendChild(meta);
			
		XmlElement saveTime = document.CreateElement(string.Empty, "savetime", string.Empty );
		XmlText timeStamp = document.CreateTextNode(DateTime.Now.ToString());
		saveTime.AppendChild(timeStamp);
		meta.AppendChild(saveTime);
	}

	public List<SaveStateBundle> LoadDocument(string name){
		xmlName = "Save/" + name + ".xml";
		List<SaveStateBundle> bundles = new List<SaveStateBundle> ();
		document = new XmlDocument ();

		document.Load (xmlName);

		XmlNodeList points = document.GetElementsByTagName ("point");
		XmlNodeList lines = document.GetElementsByTagName ("line");
		XmlNodeList planes = document.GetElementsByTagName ("plane");

		foreach(XmlNode point in points){
			SaveStateBundle b = new SaveStateBundle();
			b.SetBundleIdentifier("point");

			// Build coordinate vector from XML
			Vector3 coordinate = new Vector3(
				Convert.ToSingle(point.ChildNodes[0].ChildNodes[0].InnerText),  // x
				Convert.ToSingle(point.ChildNodes[0].ChildNodes[1].InnerText),  // y
			    Convert.ToSingle(point.ChildNodes[0].ChildNodes[2].InnerText)); // z
			// Put in bundle
			b.PutVector3(point.ChildNodes[0].Name,coordinate);
			bundles.Add (b);
		}

		foreach(XmlNode line in lines){
			SaveStateBundle b = new SaveStateBundle();
			b.SetBundleIdentifier("line");
			// Build direction vector from XML
			Vector3 direction = new Vector3(
				Convert.ToSingle(line.ChildNodes[0].ChildNodes[0].InnerText),  // x
				Convert.ToSingle(line.ChildNodes[0].ChildNodes[1].InnerText),  // y
				Convert.ToSingle(line.ChildNodes[0].ChildNodes[2].InnerText)); // z
			b.PutVector3(line.ChildNodes[0].Name,direction);
			// positonvector
			Vector3 position = new Vector3(
				Convert.ToSingle(line.ChildNodes[1].ChildNodes[0].InnerText),  // x
				Convert.ToSingle(line.ChildNodes[1].ChildNodes[1].InnerText),  // y
				Convert.ToSingle(line.ChildNodes[1].ChildNodes[2].InnerText)); // z
			b.PutVector3(line.ChildNodes[1].Name,position);
			bundles.Add (b);
		}

		foreach(XmlNode plane in planes){
			SaveStateBundle b = new SaveStateBundle();
			b.SetBundleIdentifier("plane");

			//second direction vector
			Vector3 direction_b = new Vector3(
				Convert.ToSingle(plane.ChildNodes[0].ChildNodes[0].InnerText),  // x
				Convert.ToSingle(plane.ChildNodes[0].ChildNodes[1].InnerText),  // y
				Convert.ToSingle(plane.ChildNodes[0].ChildNodes[2].InnerText)); // z
			b.PutVector3(plane.ChildNodes[0].Name,direction_b);

			//first direction vector
			Vector3 direction_a = new Vector3(
				Convert.ToSingle(plane.ChildNodes[1].ChildNodes[0].InnerText),  // x
				Convert.ToSingle(plane.ChildNodes[1].ChildNodes[1].InnerText),  // y
				Convert.ToSingle(plane.ChildNodes[1].ChildNodes[2].InnerText)); // z
			b.PutVector3(plane.ChildNodes[1].Name,direction_a);

			// positonvector
			Vector3 position = new Vector3(
				Convert.ToSingle(plane.ChildNodes[2].ChildNodes[0].InnerText),  // x
				Convert.ToSingle(plane.ChildNodes[2].ChildNodes[1].InnerText),  // y
				Convert.ToSingle(plane.ChildNodes[2].ChildNodes[2].InnerText)); // z
			b.PutVector3(plane.ChildNodes[2].Name,position);

			bundles.Add (b);
		}

		return bundles;
	}
	
	public void SaveDocument(){
		document.Save (xmlName);
	}
	
	public void PutSaveStateBundle(SaveStateBundle bundle){
		string id = bundle.GetBundleIdentifier();
		XmlElement geometryObject = document.CreateElement(bundle.GetBundleIdentifier().ToLower());
		foreach (String key in bundle.GetVectors().Keys) {
			Vector3 v = (Vector3) bundle.GetVectors() [key];
			PutVector3(geometryObject,key,v);
		}
		foreach (String key in bundle.GetFloats().Keys) {
			float f = (float) bundle.GetFloats() [key];
			PutFloat(geometryObject,key,f);
		}
		foreach (String key in bundle.GetStrings().Keys) {
			String s = (string) bundle.GetStrings() [key];
			PutString(geometryObject,key,s);
		}
		coordinateSystem.AppendChild(geometryObject);
	}
	
	private void PutVector3(XmlElement parent, string name, Vector3 vector){
		XmlElement vectorElement = document.CreateElement(name);
		
		XmlElement x = document.CreateElement("x");
		XmlText xValue = document.CreateTextNode(vector.x.ToString());
		x.AppendChild(xValue);
		vectorElement.AppendChild(x);
		
		XmlElement y = document.CreateElement("y");
		XmlText yValue = document.CreateTextNode(vector.y.ToString());
		y.AppendChild(yValue);
		vectorElement.AppendChild(y);
		
		XmlElement z = document.CreateElement("z");
		XmlText zValue = document.CreateTextNode(vector.z.ToString());
		z.AppendChild(zValue);
		vectorElement.AppendChild(z);
		
		parent.AppendChild(vectorElement);
	}
	
	private void PutString(XmlElement parent, string name, String text){
		XmlElement stringElement = document.CreateElement(name);
		XmlText textElement = document.CreateTextNode(text);
		stringElement.AppendChild(textElement);
		
		parent.AppendChild(stringElement);
	}
	
	private void PutFloat(XmlElement parent, string name, float number){
		XmlElement floatElement = document.CreateElement(name);
		XmlText numberElement = document.CreateTextNode(number.ToString());
		floatElement.AppendChild(numberElement);
		
		parent.AppendChild(floatElement);
	}
}
