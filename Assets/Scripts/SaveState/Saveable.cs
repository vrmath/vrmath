﻿using UnityEngine;
using System.Collections;


/* @ Peter Mösenthin - Hochschule Rhein-Waal
 * 
 * The SaveStateManager handles game gameObjects that want to save their state.
 * 
 */
namespace SaveState{

public interface Saveable {

	//This method is called by the SaveStateManager to get a SaveStateBundle 
	SaveStateBundle SaveState();
	
	//This method is called by the SaveStateManager to restore the state of an object
	void RestoreState(SaveStateBundle bundle);
}

}
