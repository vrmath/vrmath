﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/* @ Peter Mösenthin - Hochschule Rhein-Waal
 * 
 * The SaveStateManager handles GeometryObjects that want to save their state.
 * It can be accessed directly via GetInstance()
 * 
 */
namespace SaveState {

public class SaveStateManager : MonoBehaviour {

	private static SaveStateManager saveStateManager;


	public static SaveStateManager GetInstance() {
		// If the saveStateManager doesn't already exist, we need to create it
		if (!saveStateManager) {
			// Because the SaveStateManager is a component, we have to create a GameObject to attach it to.
			GameObject saveStateManagerObject = new GameObject("SaveStateManager");
			// Add the SaveStateManager component, and set it as the saveStateManager
			saveStateManager = saveStateManagerObject.AddComponent<SaveStateManager>();
			DontDestroyOnLoad(saveStateManager);
		}
		return saveStateManager;
	}

	public void Start(){
		
	}

	public void LoadCoordinateSystem(string name, CoordinateSystem cs){
		Debug.Log ("Loading state for " + name);
		XMLHelper xmlhelper = new XMLHelper();
		List<SaveStateBundle> bundles = xmlhelper.LoadDocument (name);
		foreach (SaveStateBundle b in bundles) {
			if(b.GetBundleIdentifier().Equals("point")){
				Point po = new Point();
				po.RestoreState(b);
				po.Draw();
				cs.LinkGeometryObject(po);
			}

			if(b.GetBundleIdentifier().Equals("line")){
				Line l = new Line();
				l.RestoreState(b);
				l.Draw();
				cs.LinkGeometryObject(l);
			}
			
			if(b.GetBundleIdentifier().Equals("plane")){
				Plane pl = new Plane();
				pl.RestoreState(b);
				pl.Draw();
				cs.LinkGeometryObject(pl);
			}
		}
	}

	public void SaveCoordinateSystem(CoordinateSystem cs){
		SaveGeometryObjectsList(cs.GetGeometryObjects(), cs.GetRootGameObject().name);
	}
	
	public void SaveGeometryObjectsList(List<GeometryObject> objectList, string name){
		Debug.Log ("Saving state for " + objectList.Count + " Saveables with name: " + name);
		XMLHelper xmlhelper = new XMLHelper();
		xmlhelper.InitDocument(name);
		foreach (GeometryObject o in objectList) {
			xmlhelper.PutSaveStateBundle(o.SaveState());
		}
		xmlhelper.SaveDocument();
	}

}

}
