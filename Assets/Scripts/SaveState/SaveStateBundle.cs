﻿using UnityEngine;
using System.Collections;
using System;


/* @ Peter Mösenthin - Hochschule Rhein-Waal
 * 
 * The SaveStateBundle is an Object that stores information
 * a game object wants to store so it can be accessed easily
 * by the SaveStateManager.
 * 
 */
namespace SaveState {

public class SaveStateBundle {

	private String bundleIdentifier;

	private Hashtable vector3List = new Hashtable();
	private Hashtable stringList = new Hashtable();
	private Hashtable floatList = new Hashtable();

	//Set the identifier to be able to find the bundle later
	public void SetBundleIdentifier(String bundleIdentifier){
		this.bundleIdentifier = bundleIdentifier;
	}
	public string GetBundleIdentifier(){
		return this.bundleIdentifier;
	}

	//Accessors for a bunch of content

	public void PutString(String name,String content){
		stringList.Add (name, content);
	}

	public String GetString(String name){
		if (stringList.ContainsKey (name)) {
			return (String) (stringList [name]);
		} else {
			return null;
		}
	}

	public void PutVector3(String name, Vector3 vector){
		vector3List.Add (name, vector);
	}

	public Vector3 GetVector3(String name){
		if (vector3List.ContainsKey (name)) {
			return (Vector3) (vector3List [name]);
		} else {
			return new Vector3(0,0,0);
		}
	}

	public Hashtable GetVectors(){
		return vector3List;
	}
	
	public Hashtable GetFloats(){
		return floatList;
	}
	
	public Hashtable GetStrings(){
		return stringList;
	}
		

	public void PutFloat(String name, float number){
		floatList.Add (name, number);
	}
	public float GetFloat(String name){
		if (floatList.ContainsKey (name)) {
			return (float) (floatList [name]);
		} else {
			return 0f;
		}
	}

}
}
