﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SaveState {

/*
 * This script test save and load functionality. Simply add it to a gameObject
 * and tick one of the testcases in the inspector. 
 */
public class SaveStateTest : MonoBehaviour {

	public bool startSaveTest = false;
	public bool startLoadTest = false;

	public bool buildTestGeometryOnStart = false;
	
	CoordinateSystem cs;

	// Use this for initialization
	void Start () {
		cs = CoordinateSystems.cs0;
		
		//cs = GeometryFactory.CreateCoordinateSystem(new Vector3(1,1,1), 0);
		if(buildTestGeometryOnStart){
			Point point2 = GeometryFactory.CreatePoint(new Vector3(7,2,7));
			point2.Draw();
			cs.LinkGeometryObject(point2);

			Point point = GeometryFactory.CreatePoint(new Vector3(7,2,6));
			point.Draw();
			cs.LinkGeometryObject(point);
		
			Line line = GeometryFactory.CreateLineFromPoints(new Vector3(1,2,3), new Vector3(9,9,9));
			line.Draw();
			cs.LinkGeometryObject(line);
			
			Plane plane = GeometryFactory.CreatePlaneFromPoints(new Vector3(5,5,5), new Vector3(8,3,5), new Vector3(7,7,7));
			plane.Draw();
			cs.LinkGeometryObject(plane);
		
			Debug.Log("SaveStateTest: Added " + cs.GetGeometryObjects().Count.ToString() + " GeometryObjects");
		} else {
			Debug.Log ("SaveStateTest: No CoordinateSystem set");
		}
	}
	
	// Update is called once per frame
	void Update () {

		// set to true through the inspector
		if (startSaveTest) {
			//stop test so it doesnt run twice
			startSaveTest = false;
			//Attempt to save State
			Debug.Log ("SaveStateTest: Starting Save Test for " + cs.GetRootGameObject().name + " with " + cs.GetGeometryObjects().Count.ToString() + " Objects");
			SaveStateManager.GetInstance().SaveCoordinateSystem(cs);
		}
		
		// set to true through the inspector
		if (startLoadTest) {
			//stop test so it doesnt run twice
			startLoadTest = false;
			//Attempt to load State
			SaveStateManager.GetInstance().LoadCoordinateSystem("CoordinateSystem0",cs);
		}
	}
}

}
