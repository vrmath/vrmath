﻿using UnityEngine;
using System.Collections;

public class TestGeometry2 : MonoBehaviour {

	float scale = 1f;

	// Use this for initialization
	void Start () {
		Point point1 = GeometryFactory.CreatePoint(new Vector3(1f,2f,0f));
		point1.SetScale (scale);
		point1.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (point1);

		
		Point point2 = GeometryFactory.CreatePoint(new Vector3(2f,4f,-1f));
		point2.SetScale (scale);
		point2.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (point2);
		
		Point point3 = GeometryFactory.CreatePoint(new Vector3(3f,6f,-2f));
		point3.SetScale (scale);
		point3.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (point3);
		
		Point point4 = GeometryFactory.CreatePoint(new Vector3(-1f,2f,0f));
		point4.SetScale (scale);
		point4.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (point4);
		
		Point point5 = GeometryFactory.CreatePoint(new Vector3(-2f,4f,1f));
		point5.SetScale (scale);
		point5.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (point5);

		Point point6 = GeometryFactory.CreatePoint(new Vector3(-3f,6f,2f));
		point6.SetScale (scale);
		point6.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (point6);


	}
}
