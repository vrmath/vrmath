﻿using UnityEngine;
using System.Collections;

public class CharackterMovement : MonoBehaviour {
	
	// These are used to modify the player movement speed, and rotation speed.
	public float PlayerMovementSpeed = 30;
	public float PlayerRotationSpeed = 180;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Movement();
	}
	
	void Movement()
	{
		// This line is for vertical movement, right now its on the Z AXIS.
		transform.Translate(0,0,Input.GetAxis("Vertical") * Time.deltaTime * PlayerMovementSpeed);
		
		// This line is for horizontal movement, right now its on the X AXIS. When combined with vertical movement it can be used for Strafing.
		transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * PlayerMovementSpeed,0,0);
		
		// This line is for rotation, right now its on the Y AXIS. 
		transform.Rotate(0,Input.GetAxis("RightStick") * Time.deltaTime * PlayerRotationSpeed,0);
	}
}
