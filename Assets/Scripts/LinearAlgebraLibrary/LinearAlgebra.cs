﻿/* @ Marco Pleines - Hochschule Rhein-Waal
 * 
 * This script of the library contains methods to work with geometric objects of the linear Algebra.
 * This script makes use of the classes Point.cs, Line.cs, Plane.cs, Sphere.cs
 * Some functions came out of this source: http://wiki.unity3d.com/index.php/3d_Math_functions
 * 
 */

/* Methods about Calculations to be implemented:
 * (All Tabs indicate that the method has been implemented)
 * 
 * Vectors & Lines:
 * 
 * 		Length of a Vector (Unity3d -> Vector3.magnitude)
 * 		Distance between point and line
 * 		Distance between two points
 * 		Check parallelism between to lines
 * 		Check perpendicular of two lines
 * 		Distance between two parallel lines
 * 		Intersection of two lines (source Math3d.cs)
 * Is there a specific point on a specific line? (linear equation system or an alternative needed) (maybe it is fair enough to display the point)
 * 		Unit vector (Einheitsvektor)  (Unity3D -> Vector3.normalized)
 * Angle of two cutting lines
 * 
 * Optional:
 * Testing lines: skew,...
 * Linearly dependent (Lineare Abhängigkeit)
 * Vector projection
 * Linear Map (Lineare Abbildung)
 * 
 * Planes & Points & Lines:
 * 
 * 		Intersection line between two planes
 * 		Intersection point between line and plane
 * 
 * Optional:
 * Shortest distance between point and plane
 * Angle between line and plane
 */

using UnityEngine;
using System.Collections;

public class LinearAlgebra : MonoBehaviour {

	// Abstand von zwei Punkten
	// Distance between two points
	public static float DistanceBetweenTwoPoints(Point pointA, Point pointB){
		Vector3 vector = pointA.GetCoordinate ()- pointB.GetCoordinate ();
		return vector.magnitude;
	}

	// Abstand einer Geraden zu einem Punkt
	// Distance between line and point
	public static float DistanceBetweenLinePoint (Line line, Point point){
		Vector3 crossProd = Vector3.Cross (line.GetDirectionVector(), line.GetPositionVector() - point.GetCoordinate());
		float distance = crossProd.magnitude / line.GetDirectionVector ().magnitude;
		return distance;
	}

	// Abstand zweier paralleler Geraden
	// Distance between two lines
	public static float DistanceBetweenTwoLines(Line lineA, Line lineB){
		Vector3 crossProd = Vector3.Cross (lineA.GetDirectionVector(), lineA.GetPositionVector () - lineB.GetPositionVector ());
		float distance = crossProd.magnitude / lineA.GetDirectionVector().magnitude;
		return distance;
	}

	// Überprüfen der Parallelität von zwei Geraden
	// Check for parallelism of two lines
	public static bool CheckParallelismTwoLines(Line lineA, Line lineB){
		if (Vector3.Normalize (lineA.GetDirectionVector()) == Vector3.Normalize (lineB.GetDirectionVector())) {
			return true;
		} else {
			return false;
		}
	}

	// Sind die zwei Geraden orthogonal zueinander?
	// Is lineA perpendicular to lineB ?
	public static bool CheckPerpendicularOfTwoLines(Line lineA, Line lineB){
		if (Vector3.Dot (lineA.GetDirectionVector (), lineB.GetDirectionVector ()) == 0) {
			return true;
		} else {
			return false;
		}
	}

	// Schnittpunkt von zwei Geraden berechnen
	// Intersection point of two lines
	public static bool IntersectionBetweenTwoLines(out Point intersectionPoint, Line lineA, Line lineB){
		
		intersectionPoint = null;
		
		Vector3 lineVec3 = lineB.GetPositionVector() - lineA.GetPositionVector();
		Vector3 crossVec1and2 = Vector3.Cross(lineA.GetDirectionVector(), lineB.GetDirectionVector());
		Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineB.GetDirectionVector());
		
		float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);
		
		//Lines are not coplanar. Take into account rounding errors.
		if((planarFactor >= 0.00001f) || (planarFactor <= -0.00001f)){
			
			return false;
		}
		
		//Note: sqrMagnitude does x*x+y*y+z*z on the input vector.
		float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
		
		if((s >= 0.0f) && (s <= 1.0f)){
			
			intersectionPoint.SetCoordinate(lineA.GetPositionVector() + (lineA.GetDirectionVector() * s));
			return true;
		}
		
		else{
			return false;       
		}
	}

	// Line of intersection between two Planes
	// Schnittgerade zwischen zwei Ebenen
	public static bool IntersectionLineBetweenTwoPlanes(out Line intersectionLine, Plane planeA, Plane planeB){
	// 	public static bool IntersectionLineBetweenTwoPlanes(out Vector3 linePoint, out Vector3 lineVec, Vector3 plane1Normal, Vector3 plane1Position, Vector3 plane2Normal, Vector3 plane2Position){

		intersectionLine = null;
		//We can get the direction of the line of intersection of the two planes by calculating the 
		//cross product of the normals of the two planes. Note that this is just a direction and the line
		//is not fixed in space yet. We need a point for that to go with the line vector.
		intersectionLine.SetDirectionVector(Vector3.Cross (planeA.GetNormalVector (), planeB.GetNormalVector ()));
		
		//Next is to calculate a point on the line to fix it's position in space. This is done by finding a vector from
		//the plane2 location, moving parallel to it's plane, and intersecting plane1. To prevent rounding
		//errors, this vector also has to be perpendicular to lineDirection. To get this vector, calculate
		//the cross product of the normal of plane2 and the lineDirection.		
		Vector3 ldir = Vector3.Cross(planeB.GetNormalVector(), intersectionLine.GetDirectionVector());		
		
		float denominator = Vector3.Dot(planeA.GetNormalVector(), ldir);
		
		//Prevent divide by zero and rounding errors by requiring about 5 degrees angle between the planes.
		if(Mathf.Abs(denominator) > 0.006f){
			
			Vector3 plane1ToPlane2 = planeA.GetPositionVector() - planeB.GetPositionVector();
			float t = Vector3.Dot(planeA.GetNormalVector(), plane1ToPlane2) / denominator;
			intersectionLine.SetPositionVector(planeB.GetPositionVector() + t * ldir);
			
			return true;
		}
		
		//output not valid
		else{
			return false;
		}
	}


	// Intersection point between line and plane
	// Schnittpunkt einer Geraden und einer Ebene
	public static bool LinePlaneIntersection(out Point point, Line line, Plane plane){
		
		float length;
		float dotNumerator;
		float dotDenominator;
		Vector3 vector;
		point = null;

		//calculate the distance between the linePoint and the line-plane intersection point
		dotNumerator = Vector3.Dot ((plane.GetPositionVector() - line.GetPositionVector()), plane.GetNormalVector());
		dotDenominator = Vector3.Dot (line.GetDirectionVector(), plane.GetNormalVector());
		
		//line and plane are not parallel
		if (dotDenominator != 0.0f) {
			length = dotNumerator / dotDenominator;
		
			//create a vector from the linePoint to the intersection point
			vector = SetVectorLength (line.GetDirectionVector(), length);
			
			//get the coordinates of the line-plane intersection point
			point.SetCoordinate(line.GetPositionVector() + vector);	
			
			return true;	
		}
		//output not valid
		else{
			return false;
		}
	}

	//create a vector of direction "vector" with length "size"
	public static Vector3 SetVectorLength(Vector3 vector, float size){
			
		//normalize the vector
		Vector3 vectorNormalized = Vector3.Normalize(vector);
			
		//scale the vector
		return vectorNormalized *= size;
	}

}