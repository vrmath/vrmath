﻿using UnityEngine;
using System.Collections;


public class TestMathFunctions : MonoBehaviour {



	void Start () {
	
		Point pointA = new Point(new Vector3 (10, 10, 0));
		pointA.Draw ();

				// Test 1: Vector3 Log
				//Vector3 einfacheAusgabe = new Vector3 (1, 2, 3);
				//Debug.Log ("Test 1" + einfacheAusgabe);
				// Funktioniert

				// Test 2: Vector3 Subtraktion
				//Vector3 minusRechnung = new Vector3 (3, 2, 1);
				//Vector3 ergebnisVektor = new Vector3 (0, 0, 0);
				//ergebnisVektor = minusRechnung - einfacheAusgabe;
				//Debug.Log ("Test 2 Substraktion: " + "Vektor 1: " + minusRechnung + " Vektor 2 : " + einfacheAusgabe + " Ergebnis: " + ergebnisVektor);
				//Funktionert

				// Test 3: Betrag eines Vektors
				//Debug.Log("Test 3 Betrag/Länge: " + einfacheAusgabe.magnitude);
				// Funktioniert

				// Test 4: Abstand zweier Punkte
				//Debug.Log ("Test 4 Abstand zweier Punkte): " + LibPointLine.DistanceBetweenTwoPoints (new Vector3 (4, 5, 6), new Vector3 (2, 1, 2)));
				// Funktioniert

				// Test 5: Distanz von Punkt zur Geraden
				//LibLine line = new LibLine (new Vector3 (3,5,3), new Vector3 (1,0,1));
				//Vector3 point = new Vector3(5,3,-2);
				//Debug.Log ("Test 5: " + line.GetPointA());
				//Debug.Log ("Test 5: " + line.GetPointB());
				//Debug.Log ("Test 5: " + line.GetDirectionVector());
				//Debug.Log ("Distanz: " + line.DistanceToPoint (point));
				// Funktioniert, habe folgende Werte getestet: http://www.frustfrei-lernen.de/mathematik/abstand-punkt-gerade.html

				// Test 6: Sind die zwei Geraden parallel???
				//LibLine lineA = new LibLine (new Vector3 (2, 2, 4), new Vector3 (4, 1, 2));
				//LibLine lineB = new LibLine (new Vector3 (4, 4, 8), new Vector3 (8, 2, 4));
				//Debug.Log ("Test 6: " + lineA.GetDirectionVector ());
				//Debug.Log ("Test 6: " + lineB.GetDirectionVector ());
				//float test = lineA.GetDirectionVector () [0] % lineB.GetDirectionVector () [0];
				//Debug.Log (test);
				//Debug.Log ("Test 6, die Geraden sind parallel: " + LibPointLine.CheckParallelismOfTwoLines (lineA, lineB));
				// Funktioniert, denn es werden die Einheitsvektoren verglichen

				// Test 7: Distanz zwischen zweier paralleler Geraden aus Test 6
				//float distanz = LibPointLine.DistanceBetweenTwoLines (lineA, lineB);
				//Debug.Log (distanz);
				// (Funktioniert), ich habe es aber nicht nachgerechnet

				// Test 8: Sind diese zwei Geraden orthogonal?
				//LibLine lineX = new LibLine (new Vector3 (1,7,5), new Vector3 (1,4,2));
				//LibLine lineY = new LibLine (new Vector3 (2,3,5), new Vector3 (3,6,2));
				//Debug.Log ("Test 8: Sind die Geraden orthogonal zueinander? " + LibPointLine.CheckPerpendicularOfTwoLines (lineX, lineY));
				//Debug.Log (lineX.GetDirectionVector());
				//Debug.Log (lineY.GetDirectionVector());
				// Funktioniert

				// Test 9: Schnittpunkt zweier Geraden im R3
				//Vector3 intersection = new Vector3();
				//LibLine lineA = new LibLine (new Vector3 (4, 2, 8), new Vector3 (5, -2, 5));
				//LibLine lineB = new LibLine (new Vector3 (5, 8, 21), new Vector3 (4, 7, 16));
				//bool a = LibPointLine.IntersectionBetweenTwoLines (out intersection, lineA.GetPointA(), lineA.GetDirectionVector(), lineB.GetPointB(), lineB.GetDirectionVector());
				//Debug.Log (intersection);
				// Funktioniert, die kopierte Methode funktioniert, aber wenn ich diese nach meinen Vorstellungen anpasse, damit man nur lineA und lineB übergeben muss klappt das nicht


				//bool b = LibPointLine.IntersectionBetweenTwoLines(out intersection, lineA, lineB);
				//Debug.Log (intersection);
			

	}
}