﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* @ Marco Pleines - Hochschule Rhein-Waal */

public class SelectGeometry : MonoBehaviour {

	int csIndex;
	bool updateEnabled = false;
	bool selectionDone = false;
	List<GeometryObject> geometryObjects;
	int selection = 0;
	bool firstSelection = true;

	// Use this for initialization
	void Start () {
		// Observe Select_Object called by Interface Modify or Compute
		NotificationCenter.DefaultCenter ().AddObserver (this, "Select_Object");
	}
	
	// Update is called once per frame
	void Update () {
		if(updateEnabled){

			// selection starts at 0
			if(firstSelection){
				geometryObjects[selection].Select();
				firstSelection = false;
			}
			// select geometry backwards
			if(Input.GetButtonDown ("360_LeftBumper"))
			{
				Debug.Log ("Left Bumper triggered");
				// shift left
				if(selection == 0){
					geometryObjects[selection].DeSelect();
					selection = geometryObjects.Count-1;
					geometryObjects[selection].Select();

				} else {
					geometryObjects[selection].DeSelect();
					selection--;
					geometryObjects[selection].Select();
				}


			}
			
			// select geometry forward
			if(Input.GetButtonDown ("360_RightBumper"))
			{	
				// shift right
				if(selection == geometryObjects.Count-1){
					geometryObjects[selection].DeSelect();
					selection = 0;
					geometryObjects[selection].Select();

				} else {
					geometryObjects[selection].DeSelect();
					selection++;
					geometryObjects[selection].Select();
				}
			}

			// disbale selection
			if(Input.GetButtonDown("360_AButton") || Input.GetButtonDown("360_YButton")){
				Debug.Log("Get Type : " + geometryObjects[selection].GetType().ToString());

				switch (geometryObjects[selection].GetType().ToString()){
				case "Point":
					// call create point interface
					/*
					Point point = (Point)geometryObjects[selection];
					NotificationCenter.DefaultCenter().PostNotification(this, "Create_Point", point);
					Destroy (geometryObjects[selection].GetGameObject());
					geometryObjects[selection] = null;
					geometryObjects.RemoveAt(selection);
					System.GC.Collect();
					*/
					break;
				case "Line":
					break;
				case "Plane":
					break;
				case "Sphere":
					break;
				}

				selectionDone = true;

			}

			// if done
			if(selectionDone){
				Debug.Log ("Selection done");
				// if modify
				//NotificationCenter.DefaultCenter().PostNotification(this, "", );
				// if selection
				//NotificationCenter.DefaultCenter().PostNotification(this, "", );
				updateEnabled = false;
				//geometryObjects[selection].DeSelect();
			}
		}
	}

	void Select_Object (Notification notification) {
		updateEnabled = true;
		csIndex = (int)notification.data;
		Debug.Log (csIndex.ToString ());

		// get specific geometryObjectsList
		switch (csIndex){
		case 0:
			geometryObjects = CoordinateSystems.cs0.GetGeometryObjects();
			break;
		case 1:
			geometryObjects = CoordinateSystems.cs1.GetGeometryObjects();
			break;
		case 2:
			geometryObjects = CoordinateSystems.cs2.GetGeometryObjects();
			break;
		}
		Debug.Log ("Counted geometry objects : " + geometryObjects.Count);
		firstSelection = true;
		selectionDone = false;
		selection = 0;
	}
}
