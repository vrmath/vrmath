﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal
 * 
 * This class covers the input of creating a point
 * 
 */

public class InterfaceInputPoint : MonoBehaviour {
	
	// Member
	TextMesh textMesh;
	string red = "#ff0000";
	string white = "#ffffffff";
	string[] selectedColor = new string[3];
	public Vector3 point = new Vector3(1,1,1);
	int selectionCounter = 0;
	bool waitUp = false;
	bool waitDown = false;
	float waitTimeSelection = 0.1f;
	
	
	// Use this for initialization
	void Start () {
		
		// initialize TextMesh
		textMesh = gameObject.GetComponent(typeof(TextMesh)) as TextMesh;
		
		// Define default selection case		
		selectedColor[0] = red;
		selectedColor[1] = white;
		selectedColor[2] = white;
		
		// default string, x is selected
		textMesh.text = "Point: ( <color=" + red + ">" + point.x + "</color> / <color=" + white + ">" + point.y + "</color>" + " / <color=" + white + ">" + point.z + "</color> )";
	}
	
	// Update is called once per frame
	void Update () {
	
		// select values to the left
		if(Input.GetButtonDown ("360_LeftBumper"))
		{
			// shift left
			if(selectedColor[0] == red){
				selectedColor[0] = white;
				selectedColor[1] = white;
				selectedColor[2] = red;
			} else if(selectedColor[1] == red){
				selectedColor[0] = red;
				selectedColor[1] = white;
				selectedColor[2] = white;
			} else if(selectedColor[2] == red){
				selectedColor[0] = white;
				selectedColor[1] = red;
				selectedColor[2] = white;
			}
		}
		
		// select values to the right
		if(Input.GetButtonDown ("360_RightBumper"))
		{	
			// shift right
			if(selectedColor[0] == red){
				selectedColor[0] = white;
				selectedColor[1] = red;
				selectedColor[2] = white;
			} else if(selectedColor[1] == red){
				selectedColor[0] = white;
				selectedColor[1] = white;
				selectedColor[2] = red;
			} else if(selectedColor[2] == red){
				selectedColor[0] = red;
				selectedColor[1] = white;
				selectedColor[2] = white;
			}
		}
		

		// adjust values
		if(Input.GetAxis("360_Triggers")>0.051 && waitUp == false){
			// increase values
			if(selectedColor[0] == red){
				point.x++;
			} else if(selectedColor[1] == red){
				point.y++;
			} else if(selectedColor[2] == red){
				point.z++;
			}
			StartCoroutine(WaitUp (waitTimeSelection));
		}
		
		if(Input.GetAxis("360_Triggers")<-0.5 && waitDown == false){
			// decrease values
			if(selectedColor[0] == red){
				point.x--;
			} else if(selectedColor[1] == red){
				point.y--;
			} else if(selectedColor[2] == red){
				point.z--;
			}
			StartCoroutine(WaitDown (waitTimeSelection));
		}
		
		// set new text
		textMesh.text = "Point: ( <color=" + selectedColor[0] + ">" + point.x + "</color> / <color=" + selectedColor[1] + ">" + point.y + "</color>" + " / <color=" + selectedColor[2] + ">" + point.z + "</color> )";
		
		// post new Vector to GeoProcessor.cs
		if(Input.GetButtonDown("360_AButton")){
			NotificationCenter.DefaultCenter().PostNotification(this, "CreatePoint", point);
		}
	}
	
	
	// Bumper's input delay
	IEnumerator WaitUp(float waitTime) {
		waitUp = true;
		yield return new WaitForSeconds(waitTime);
		waitUp = false;
	}
	IEnumerator WaitDown(float waitTime) {
		waitDown = true;
		yield return new WaitForSeconds(waitTime);
		waitDown = false;
	}
}