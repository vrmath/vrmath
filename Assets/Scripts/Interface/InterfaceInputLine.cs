﻿using UnityEngine;
using System.Collections;

public class InterfaceInputLine : MonoBehaviour {
	
	// Member
	public GameObject[] selectableTextObjects;
	int selection = 0;
	// Delay
	bool waitUp = false;
	bool waitDown = false;
	float waitTimeSelection = 0.1f;
	// line vector
	Vector3 posVector = new Vector3(1,1,1);
	Vector3 dirVector = new Vector3(1,1,1);

	// Use this for initialization
	void Start () {
		// set default selection
		SetChangedColor(0, Color.red);
	}
	
	// Update is called once per frame
	void Update () {
		UserInput();
	}
	
	
	void UserInput(){
	
		// select values to the left
		if(Input.GetButtonDown ("360_LeftBumper"))
		{
			// shift left
			if(selection == 0){
				SetChangedColor(selectableTextObjects.Length-1, Color.red);
				SetChangedColor(0, Color.white);
				selection = selectableTextObjects.Length-1;
			} else {
				// set selection color
				SetChangedColor(selection-1, Color.red);
				// change previous color
				SetChangedColor(selection, Color.white);
				selection--;
			}
		}
		
		// select values to the right
		if(Input.GetButtonDown ("360_RightBumper"))
		{	
			// shift right
			if(selection == selectableTextObjects.Length-1){
				SetChangedColor(0, Color.red);
				SetChangedColor(selectableTextObjects.Length-1, Color.white);
				selection = 0;
			} else {
				// set selection color
				SetChangedColor(selection+1, Color.red);
				// change previous color
				SetChangedColor(selection, Color.white);
				selection++;
			}
		}
		
		// adjust values
		if(Input.GetAxis("360_Triggers")>0.051 && waitUp == false){
			// increase values
			IncreaseValue (selection);
			StartCoroutine(WaitUp (waitTimeSelection));
		}
		
		if(Input.GetAxis("360_Triggers")<-0.5 && waitDown == false){
			// decrease values
			DecreaseValue (selection);
			StartCoroutine(WaitDown (waitTimeSelection));
		}
		
		// post new Vectors to GeoProcessor.cs
		if(Input.GetButtonDown("360_AButton")){
			Vector3[] posDirVectors = new Vector3[2];
			posDirVectors[0] = posVector;
			posDirVectors[1] = dirVector;
			NotificationCenter.DefaultCenter().PostNotification(this, "CreateLine", posDirVectors);
		}
	}
	
	void IncreaseValue(int selectionId){
		switch(selectionId){
		case 0:
			posVector.x++;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + posVector.x;
			break;
		case 1:
			posVector.y++;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + posVector.y;
			break;
		case 2:
			posVector.z++;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + posVector.z;
			break;
		case 3:
			dirVector.x++;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + dirVector.x;
			break;
		case 4:
			dirVector.y++;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + dirVector.y;
			break;
		case 5:
			dirVector.z++;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + dirVector.z;
			break;
		}
	}
	
	void DecreaseValue(int selectionId){
		switch(selectionId){
		case 0:
			posVector.x--;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + posVector.x;
			break;
		case 1:
			posVector.y--;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + posVector.y;
			break;
		case 2:
			posVector.z--;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + posVector.z;
			break;
		case 3:
			dirVector.x--;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + dirVector.x;
			break;
		case 4:
			dirVector.y--;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + dirVector.y;
			break;
		case 5:
			dirVector.z--;
			selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().text = "" + dirVector.z;
			break;
		}
	}
	
			
	void SetChangedColor(int selectionId, Color color){
		selectableTextObjects[selectionId].transform.GetComponent<TextMesh>().renderer.material.color = color;
	}
	
	// Bumper's input delay
	IEnumerator WaitUp(float waitTime) {
		waitUp = true;
		yield return new WaitForSeconds(waitTime);
		waitUp = false;
	}
	IEnumerator WaitDown(float waitTime) {
		waitDown = true;
		yield return new WaitForSeconds(waitTime);
		waitDown = false;
	}
}