﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal */
// The main duty of this component is that the text objects of the interface face towards the player.

namespace Interface{

public class LookAtPlayer : MonoBehaviour {

	//Member
	private Transform target;

	// Use this for initialization
	void Start () {
		// the root object is the player
		target = gameObject.transform.root;
		// text rotation equals player rotation
		gameObject.transform.localRotation = target.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
	}
}
}