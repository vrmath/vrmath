﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal */

/* At first this class initializes all options as TextMeshs which are mentioned in the inspector inside of Unity.
 * After that all Meshs are saved in Array so that this scipt is able to iterate through all options.
 * Next, if an option has been chosen and triggered by GamePad Input a Notfication is posted which calls the needed function.
 * Be aware: The function which is supposed to be called has to be implemented inside of OptionContrller.cs with the same name (case sensitive).
 * 
 * Adjust the frame and the background within the editor!
 */

namespace Interface{

public class InterfaceBuilder : MonoBehaviour {
	
	// Member
	// InterfaceBuilder
	public string prefix;
	public string[] options = new string[0];

	Material fontMaterial;
	Font font;
	float topBottomScale;
	float leftRightScale;
	// Frame Transforms
	Transform leftFrame;
	Transform rightFrame;
	Transform topFrame;
	Transform bottomFrame;
	Transform cornerTopLeftFrame;
	Transform cornerTopRightFrame;
	Transform cornerBottomLeftFrame;
	Transform cornerBottomRightFrame;
	
	GameObject textHolder;
	GameObject[] optionsHolderArray;
	
	// Counter/Determiner for selection
	int selection;
	// Booleans to handle the wait Coroutines
	bool waitUp = false;
	bool waitDown = false;
	
	float waitTimeSelection = 0.33f;
	
	
	void Start () {
		// load font and material
		font = Resources.Load ("3dtext/ARIAL") as Font;
		fontMaterial = Resources.Load ("3dtext/3dTextMaterial") as Material;

		
		// Instantiating a GO for clarity, it is the parent of all TextMeshes
		textHolder = new GameObject("0 TextMesh Holder");
		textHolder.transform.parent = gameObject.transform;
		textHolder.transform.position = gameObject.transform.position;
		textHolder.transform.rotation = gameObject.transform.rotation;
		textHolder.AddComponent (typeof(LookAtPlayer));

		
		// Instantiating the GOs with TextMeshes and MeshRenderer
		optionsHolderArray = new GameObject[options.Length];
		for (int i = 0; i < options.Length; i++) {
			optionsHolderArray[i] = new GameObject(options[i]);
			// make it a child of textHolder
			optionsHolderArray[i].transform.parent = textHolder.transform;
			optionsHolderArray[i].transform.position = gameObject.transform.position;
			optionsHolderArray[i].transform.Translate(new Vector3 (0f, -0.1f, 0f));
			// Add components and values
			optionsHolderArray[i].AddComponent(typeof(TextMesh));
			MeshRenderer mr = optionsHolderArray[i].GetComponent (typeof(MeshRenderer)) as MeshRenderer;
			TextMesh tm = optionsHolderArray[i].GetComponent(typeof(TextMesh)) as TextMesh;
			mr.material = fontMaterial;
			tm.font = font;
			tm.characterSize = 0.1f;
			tm.fontSize = 36;
			tm.text = options[i];
			tm.anchor = TextAnchor.MiddleCenter;
			// Text Offset
			optionsHolderArray[i].transform.Translate(new Vector3(0, i*.5f - i, 0));
		}
		
		// Whole Text Offset
		textHolder.transform.Translate (new Vector3 (1,1,0));

		// Option 0 selected
		selection = 0;
		optionsHolderArray[selection].renderer.material.color = Color.red;
	}
	
		// Update
		void Update () {
			// GamePad Input
			// Select upwards
			if (Input.GetAxis ("360_VerticalDPAD") == 1 && waitUp == false) {
				if(selection == 0){
					optionsHolderArray[selection].renderer.material.color = Color.white;
					optionsHolderArray[optionsHolderArray.Length-1].renderer.material.color = Color.red;
					selection = optionsHolderArray.Length-1;
				} else {
					optionsHolderArray[selection].renderer.material.color = Color.white;
					optionsHolderArray[selection-1].renderer.material.color = Color.red;
					selection--;
				}
				// wait, so that the option selection doesn't speed through
				StartCoroutine(WaitUp (waitTimeSelection));
			}
			
			// Select downwards
			if (Input.GetAxis ("360_VerticalDPAD") < 0 && waitDown == false){
				if(selection == optionsHolderArray.Length-1){
					optionsHolderArray[selection].renderer.material.color = Color.white;
					optionsHolderArray[0].renderer.material.color = Color.red;
					selection = 0;
				} else {
					optionsHolderArray[selection].renderer.material.color = Color.white;
					optionsHolderArray[selection+1].renderer.material.color = Color.red;
					selection++;
				}
				// wait, so that the option selection doesn't speed through
				StartCoroutine(WaitDown (waitTimeSelection));
			}
			// Trigger Button and post notification to call the method
			// Be aware: OptionController.cs is in charge of all available functions concerning this interface!
			if (Input.GetButtonDown ("360_AButton")) {
				NotificationCenter.DefaultCenter().PostNotification(this, prefix + options[selection]);
			}
		}
		
		// Vertical DPAD Input delay
		IEnumerator WaitUp(float waitTime) {
			waitUp = true;
			yield return new WaitForSeconds(waitTime);
			waitUp = false;
		}
		IEnumerator WaitDown(float waitTime) {
			waitDown = true;
			yield return new WaitForSeconds(waitTime);
			waitDown = false;
		}
}
}