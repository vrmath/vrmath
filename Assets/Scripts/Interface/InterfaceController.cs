﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal */

public class InterfaceController : MonoBehaviour {

	// Public Array to load all interfaces in a specific order
	public Transform[] interfaceMenus;
	public Vector3 interfaceOffset = new Vector3(0,0,0);
	public Vector3 createPointInterfaceOffset = new Vector3 (0, 0, 0);
	public Vector3 createLineInterfaceOffset = new Vector3(0,0,0);
	public Vector3 createPlaneInterfaceOffset = new Vector3(0,0,0);
	bool interfaceCreated = false;
	Transform interfaceHolder;
	int currentInterface = 99;
	
	void Start(){
		// Create Interface Notifications called by the individual interfaces
		NotificationCenter.DefaultCenter ().AddObserver (this, "Create");
		NotificationCenter.DefaultCenter ().AddObserver (this, "Create_Point");
		NotificationCenter.DefaultCenter ().AddObserver (this, "CreatePoint");
		NotificationCenter.DefaultCenter ().AddObserver (this, "Create_Line");
		NotificationCenter.DefaultCenter ().AddObserver (this, "CreateLine");
		NotificationCenter.DefaultCenter ().AddObserver (this, "Create_Plane");
		NotificationCenter.DefaultCenter ().AddObserver (this, "CreatePlane");
	}
	
	void Update ()
	{
		UserInputs();
	}
	
	// This function handles the Inputs from the buttons on the controller
	void UserInputs()
	{
		
		// TODO This button opens the previous menus
		if(Input.GetButtonDown ("360_BButton"))
		{
			if(interfaceCreated == true && currentInterface != 0){
				DestroyInterface();
				//InstantiateInterface(currentInterface--);
				// TODO still a problem going on
				InstantiateInterface(0);
			}else if(!interfaceCreated && currentInterface == 0){
				NotificationCenter.DefaultCenter().PostNotification(this, "ClearLastSelection");
			}
		}
		
		// This Button instantiates and destroys the menu interfaces
		if(Input.GetButtonDown ("360_YButton"))
		{
			if(interfaceCreated == false){
				// if there is no interface, create the main interface
				InstantiateInterface(0);
			} else {
				// else, destroy the current one
				DestroyInterface();
			}
		}
	}
	
	void InstantiateInterface(int id){
		// instantiate interface
		interfaceHolder = Instantiate(interfaceMenus[id], gameObject.transform.position, gameObject.transform.localRotation) as Transform;
		// make it a child of the charackter
		interfaceHolder.transform.parent = gameObject.transform;
		
		// translation to demanded spot of vision
		switch (id){
		case 0:
			interfaceHolder.transform.Translate(interfaceOffset);
			break;
		case 1:
			interfaceHolder.transform.Translate(interfaceOffset);
			break;
		case 2:
			interfaceHolder.transform.Translate(createPointInterfaceOffset);
			break;
		case 3:
			interfaceHolder.transform.Translate(createLineInterfaceOffset);
			break;
		case 4 :
			interfaceHolder.transform.Translate(createPlaneInterfaceOffset);
			break;
		case 5:
			interfaceHolder.transform.Translate(interfaceOffset);
			break;
		}

		// all childs need to be rotated correctly
		//for(int i = 0; interfaceHolder.transform.GetChild(0).childCount > i; i++){
		//	interfaceHolder.transform.GetChild(0).GetChild(i).transform.localRotation = gameObject.transform.rotation;
		//}
		
		// variables for handling the interfaces
		interfaceCreated = true;
		currentInterface = id;
	}

	void InstantiateInterfacePoint(Vector3 coordinate){
		// instantiate interface
		interfaceHolder = Instantiate(interfaceMenus[2], gameObject.transform.position, gameObject.transform.localRotation) as Transform;
		// make it a child of the charackter
		interfaceHolder.transform.parent = gameObject.transform;

		interfaceHolder.transform.Translate(createPointInterfaceOffset);

		InterfaceInputPoint iip = interfaceHolder.GetChild (0).GetComponent<InterfaceInputPoint> ();
		iip.point = coordinate;

		// variables for handling the interfaces
		interfaceCreated = true;
		currentInterface = 2;
	}

	void DestroyInterface(){
		Destroy (interfaceHolder.gameObject);
		interfaceCreated = false;
		currentInterface = 99;
	}
	
	// Messages by NfCenter
	// Load Create Interface
	void Create(){
		DestroyInterface();
		InstantiateInterface(1);
	}
	
	// Load Create Point Interface
	void Create_Point(Notification notification){
		DestroyInterface();
		// if notification was sent by modifiy
		if (notification.sender.name == "InterfaceOptionController") {
			// load the InputPoint Interface to modify selected geometry
			Point point = (Point)notification.data;
			InstantiateInterfacePoint(point.GetCoordinate());
		} else {
			InstantiateInterface (2);
		}
	}
	
	// Destroy CreatePointInterface if a point has been created
	void CreatePoint(){
		DestroyInterface();
		Create();
	}
	
	// Load Create Point Interface
	void Create_Line(){
		DestroyInterface();
		InstantiateInterface(3);
	}
	
	// Destroy CreatePointInterface if a point has been created
	void CreateLine(){
		DestroyInterface();
		Create();
	}
	
	// Load Create Point Interface
	void Create_Plane(){
		DestroyInterface();
		InstantiateInterface(4);
	}
	
	// Destroy CreatePointInterface if a point has been created
	void CreatePlane(){
		DestroyInterface();
		Create();
	}
}