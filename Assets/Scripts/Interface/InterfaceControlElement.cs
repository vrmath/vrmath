﻿using UnityEngine;
using System.Collections;

/* @ Marco Pleines - Hochschule Rhein-Waal */

/*
 * This class takes care of enabling the control element which is in charge of the interface
 * 
 * 
 * 
 */

public class InterfaceControlElement : MonoBehaviour {

	// Member
	public GameObject interfaceRenderTextureSpot;
	public int csIndex;

	// Use this for initialization
	void Start () {
		NotificationCenter.DefaultCenter ().AddObserver (this, "EnableInterface");
		NotificationCenter.DefaultCenter ().AddObserver (this, "DisableInterface");
	}
	
	// Update is called once per frame
	void Update () {

	}

	void EnableInterface(Notification notification){
		GameObject coordinateSystem = (GameObject)notification.data;
		string comparisonString = "CoordinateSystem" + csIndex.ToString();
		if (coordinateSystem.name == comparisonString) {
			Debug.Log ("true if??? just can't activate");
			interfaceRenderTextureSpot.SetActive(true);
		}
	}

	void DisableInterface(Notification notification){
		GameObject coordinateSystem = (GameObject)notification.data;
		string comparisonString = "CoordinateSystem" + csIndex.ToString();
		if (coordinateSystem.name == comparisonString) {
			interfaceRenderTextureSpot.SetActive(false);
		}
	}
}
