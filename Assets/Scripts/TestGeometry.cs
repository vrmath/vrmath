﻿using UnityEngine;
using System.Collections;

public class TestGeometry : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		
		// Test Geometry
		/*
		Point point = GeometryFactory.CreatePoint(new Vector3(6.75f,1f,-5f));
		point.SetScale (0.5f);
		point.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (point);
		point.Select();
		//point.DeSelect();

		 */
		
		Line line = GeometryFactory.CreateLineFromPoints(new Vector3(6.5f,1,-7.5f), new Vector3(-6.5f,1,-7.5f));
		line.Draw();
		CoordinateSystems.cs1.LinkGeometryObject (line);

		
		/*
		Plane plane = GeometryFactory.CreatePlaneFromPoints(new Vector3(6.5f,1,-7.5f), new Vector3(2,5,0), new Vector3(-1,2,4));
		plane.Draw();
		CoordinateSystems.cs2.LinkGeometryObject (plane);
		plane.Select();
		*/

		/*
		Point point2 = GeometryFactory.CreatePoint(new Vector3(1,2,1));
		point2.Draw ();
		CoordinateSystems.cs2.LinkGeometryObject (point2);
		*/
		
		Point point3 = GeometryFactory.CreatePoint(new Vector3(2,0,0));
		point3.Draw ();
		CoordinateSystems.cs1.LinkGeometryObject (point3);

		/*
		Line line2 = GeometryFactory.CreateLineFromEquation (new Vector3 (0, 0, 0), new Vector3 (5, 5, 5));
		line2.Draw ();
		CoordinateSystems.cs2.LinkGeometryObject (line2);
		line2.Select ();
		*/

	}
}
