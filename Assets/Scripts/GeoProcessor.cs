﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* @ Marco Pleines & Peter Mösenthin - Hochschule Rhein-Waal */

/*
 * The GeoProcessor handles the instantiation and linking of geometry
 * It also covers the interface's functions like save, load, modify, compute
 *
*/


public class GeoProcessor : MonoBehaviour {

	// Use this for initialization
	void Start () {
	// Add Observer
	NotificationCenter.DefaultCenter ().AddObserver (this, "Modify");
	NotificationCenter.DefaultCenter ().AddObserver (this, "Compute");
	NotificationCenter.DefaultCenter ().AddObserver (this, "Save");
	NotificationCenter.DefaultCenter ().AddObserver (this, "Load");
	NotificationCenter.DefaultCenter ().AddObserver (this, "CreatePoint");
	NotificationCenter.DefaultCenter ().AddObserver (this, "CreateLine");
	NotificationCenter.DefaultCenter ().AddObserver (this, "CreatePlane");		
	NotificationCenter.DefaultCenter ().AddObserver (this, "CreateSphere");
	}	
		
	// Observer's functions
	void Modify(Notification notification){
		// send the coordinate system's reference within the notification
		NotificationCenter.DefaultCenter().PostNotification(this, "Select_Object", notification.sender.gameObject.transform.parent.GetComponent<ReferenceOfCoordinateSystem>().csIndex);
	}

	void Compute(Notification notification){
		Debug.Log ("GeoProcessor: Compute called");
		// send the coordinate system's reference within the notification
		NotificationCenter.DefaultCenter().PostNotification(this, "Select_Object", notification.sender.gameObject.transform.parent.GetComponent<ReferenceOfCoordinateSystem>().csIndex);
	}

	void Save(Notification notification){
		Debug.Log ("GeoProcessor: Save called");
		//TODO Trigger save operation
		// send the coordinate system's reference within the notification
		NotificationCenter.DefaultCenter().PostNotification(this, "Select_Object", notification.sender.gameObject.transform.parent.GetComponent<ReferenceOfCoordinateSystem>().csIndex);
	}
	
	void Load(Notification notification){
		Debug.Log ("GeoProcessor: Load called");
		//TODO Trigger load operation
		// send the coordinate system's reference within the notification
		NotificationCenter.DefaultCenter ().PostNotification (this, "Select_Object", notification.sender.gameObject.transform.parent.GetComponent<ReferenceOfCoordinateSystem> ().csIndex);
	}
	
	void CreatePoint(Notification notification){
		// get corresponding coordinate system reference
		// in the case of creating a point: move once more up in the hierarchy
		int csReference = notification.sender.transform.parent.transform.parent.GetComponent<ReferenceOfCoordinateSystem>().csIndex;
		Point point = GeometryFactory.CreatePoint((Vector3)notification.data);
		point.Draw ();
		// link to cs
		switch (csReference){
		case 0:
			CoordinateSystems.cs0.LinkGeometryObject(point);
			break;
		case 1:	
			CoordinateSystems.cs1.LinkGeometryObject(point);
			break;
		case 2:
			CoordinateSystems.cs2.LinkGeometryObject(point);
			break;
		}
	}
	
	void CreateLine(Notification notification){
		Vector3[] vectors = new Vector3[2];
		// get notification data
		vectors = (Vector3[])notification.data;
		// get corresponding coordinate system reference
		int csReference = notification.sender.transform.parent.GetComponent<ReferenceOfCoordinateSystem>().csIndex;
		Line line = GeometryFactory.CreateLineFromEquation(vectors[0], vectors[1]);
		line.Draw();

		// link to cs
		switch (csReference){
			case 0:
			CoordinateSystems.cs0.LinkGeometryObject(line);
			break;
			case 1:	
			CoordinateSystems.cs1.LinkGeometryObject(line);
			break;
			case 2:
			CoordinateSystems.cs2.LinkGeometryObject(line);
			break;
		}
	}

	void CreatePlane(Notification notification){
		Vector3[] vectors = new Vector3[3];
		// get notification data
		vectors = (Vector3[])notification.data;
		// get corresponding coordinate system reference
		int csReference = notification.sender.transform.parent.GetComponent<ReferenceOfCoordinateSystem>().csIndex;
		Plane plane = GeometryFactory.CreatePlaneFromEquation(vectors[0], vectors[1], vectors[2]);
		plane.Draw();

		// link to cs
		switch (csReference){
			case 0:
			CoordinateSystems.cs0.LinkGeometryObject(plane);
			break;
			case 1:	
			CoordinateSystems.cs1.LinkGeometryObject(plane);
			break;
			case 2:
			CoordinateSystems.cs2.LinkGeometryObject(plane);
			break;
		}
	}
	
	void CreateSphere(){
		Debug.Log ("GeoProcessor: CreateSphere called");
		Debug.Log ("Sphere is not implemented yet!");
		//Sphere sphere = GeometryFactory.CreateSphere();
	}
}
